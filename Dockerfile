FROM circleci/android:api-27-node

ENV PATH="${PATH}:/opt/android/sdk/build-tools/29.0.2"

RUN set -xeu \
    && which adb \
    && which aapt2
